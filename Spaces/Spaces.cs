﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spaces
{
    public partial class Spaces : Form
    {
        bool fullwidth;
        public Spaces()
        {
            InitializeComponent();
            textBox2.Text = " ";
        }

        private Dictionary<char, char> letters = new Dictionary<char, char>()
            {
            #region Spaghetti
                {'\u0041', '\uff21'},
                {'\u0042', '\uff22'},
                {'\u0043', '\uff23'},
                {'\u0044', '\uff24'},
                {'\u0045', '\uff25'},
                {'\u0046', '\uff26'},
                {'\u0047', '\uff27'},
                {'\u0048', '\uff28'},
                {'\u0049', '\uff29'},
                {'\u004a', '\uff2a'},
                {'\u004b', '\uff2b'},
                {'\u004c', '\uff2c'},
                {'\u004d', '\uff2d'},
                {'\u004e', '\uff2e'},
                {'\u004f', '\uff2f'},
                {'\u0050', '\uff30'},
                {'\u0051', '\uff31'},
                {'\u0052', '\uff32'},
                {'\u0053', '\uff33'},
                {'\u0054', '\uff34'},
                {'\u0055', '\uff35'},
                {'\u0056', '\uff36'},
                {'\u0057', '\uff37'},
                {'\u0058', '\uff38'},
                {'\u0059', '\uff39'},
                {'\u005a', '\uff3a'},
                {'\u0061', '\uff41'},
                {'\u0062', '\uff42'},
                {'\u0063', '\uff43'},
                {'\u0064', '\uff44'},
                {'\u0065', '\uff45'},
                {'\u0066', '\uff46'},
                {'\u0067', '\uff47'},
                {'\u0068', '\uff48'},
                {'\u0069', '\uff49'},
                {'\u006a', '\uff4a'},
                {'\u006b', '\uff4b'},
                {'\u006c', '\uff4c'},
                {'\u006d', '\uff4d'},
                {'\u006e', '\uff4e'},
                {'\u006f', '\uff4f'},
                {'\u0070', '\uff50'},
                {'\u0071', '\uff51'},
                {'\u0072', '\uff52'},
                {'\u0073', '\uff53'},
                {'\u0074', '\uff54'},
                {'\u0075', '\uff55'},
                {'\u0076', '\uff56'},
                {'\u0077', '\uff57'},
                {'\u0078', '\uff58'},
                {'\u0079', '\uff59'},
                {'\u007a', '\uff5a'}
            #endregion
            };

        private string Wide(string text)
        {
            string output = "";

            for (var i = 0; i < text.Length; i++)
            {
                char normal = text[i];
                try
                {
                    char latin = letters[normal];
                    output += latin;
                }
                catch (KeyNotFoundException e) { output += normal; }
            }
            return output;
        }

        private void ConvOne(object sender, EventArgs e) // Convert normal
        {
            fullwidth = false;
            string txt = textBox1.Text, seperator = textBox2.Text, txtnew = "";
            for (int i = 0; i < txt.Length; i++)
            {
                if (txt.Substring(i, 1) == "\n") { txtnew += Environment.NewLine; }
                else txtnew += txt.Substring(i, 1) + seperator;
            }
            if (txtnew.EndsWith(seperator) && seperator.Length > 1)
            {
                txtnew = txtnew.Substring(0, txtnew.LastIndexOf(seperator));
            }
            textBox3.Text = txtnew;
        }

        private void ConvTwo(object sender, EventArgs e) // Convert fullwidth
        {
            fullwidth = true;
            string txt = textBox1.Text, seperator = textBox2.Text, txtnew = "";
            for (int i = 0; i < txt.Length; i++)
            {
                if (txt.Substring(i, 1) == "\n") { txtnew += Environment.NewLine; }
                else txtnew += txt.Substring(i, 1) + seperator;
            }
            if (txtnew.EndsWith(seperator) && seperator.Length > 1)
            {
                txtnew = txtnew.Substring(0, txtnew.LastIndexOf(seperator));
            }
            textBox3.Text = Wide(txtnew);
        }

        private void CopyToClipboard(object sender, EventArgs e) // Copy to clipboard
        {
            try
            {
                if (fullwidth) Clipboard.SetText(Wide(textBox3.Text));
                else Clipboard.SetText(textBox3.Text);
            }
            catch (Exception ex)
            {

            }
        }


    }
}