﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Spaces
{
    static class Program
    {
        [STAThread] // Neccessary in order to allow copying to clipboard!
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Spaces());
        }
    }
}
